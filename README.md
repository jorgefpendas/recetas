# Recetas

Este recetario tiene el objetivo de compartir recetas fácilmente con otros miembros de mi familia.

Las recetas disponibles son las siguientes: 

- [Bacalao cremoso con tomates secos y patatas](recetas/bacalaotomatessecospatatas.org)
- [Jamoncitos de pollo al curry](recetas/jamoncitospollocurry.org)
- [Risotto de setas (V)](recetas/risottosetas.org)
- [Salsa bolognesa](recetas/bolognesa.org)

Leyenda: (V), receta vegetariana; (Vn), receta vegana; (>V) o (>Vn), la receta puede hacerse vegetariana o vegana siguiendo instrucciones en la receta.

